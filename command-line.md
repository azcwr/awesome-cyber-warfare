# CLI Tools, Tips and Tricks

Knowing how to use the command line is indispensable to security professionals and software developers. Below are some good resources to start getting comfortable or find new and useful tools.

## Getting CLI Help

Command Line Interfaces (CLIs) are far more flexible and powerful than Graphical User Interfaces (GUIs) at the expense of being less discoverable. Two great resources besides the builtin man pages are:

* [explainshell.com](https://explainshell.com/) - match command-line arguments to their help text
* [showthedocs](http://showthedocs.com/)

## Getting Started

* [bash-handbook](https://github.com/denysdovhan/bash-handbook) - For those who wanna learn Bash
* [awesome-bash-commands](https://github.com/joseluisq/awesome-bash-commands) - A curated list of awesome Bash useful commands. Inspired by awesome-shell and bash-handbook.
* [awesome-shell](https://github.com/alebcay/awesome-shell) - A curated list of awesome command-line frameworks, toolkits, guides and gizmos.
* [awesome-cli-apps](https://github.com/agarrharr/awesome-cli-apps) - A curated list of command line apps
* [awesome-command-line-apps](https://github.com/herrbischoff/awesome-command-line-apps) - Use your terminal shell to do awesome things.
* [Awesome-Terminal-Commands](https://github.com/CodeMaxx/Awesome-Terminal-Commands) - An awesome resource listing and explaining various commonly used *nix commands
* [awesome-console-services](https://github.com/chubin/awesome-console-services) - A curated list of awesome console services (reachable via HTTP, HTTPS and other network protocols)

## Platform Specific

* [awesome-macos-command-line](https://github.com/herrbischoff/awesome-macos-command-line) - Use your macOS terminal shell to do awesome things.
* [awesome-windows-command-line](https://github.com/Awesome-Windows/awesome-windows-command-line) - Use your Windows terminal to do awesome things.

## Terminal Customization

* [terminals-are-sexy](https://github.com/k4m4/terminals-are-sexy) - A curated list of Terminal frameworks, plugins & resources for CLI lovers.
* [awesome-terminal-fonts](https://github.com/gabrielelana/awesome-terminal-fonts) - Tools and instructions on how to have awesome symbols in a terminal with a monospace font
* [awesome-zsh-plugins](https://github.com/unixorn/awesome-zsh-plugins) - A collection of ZSH frameworks, plugins & themes inspired by the various awesome list collections out there.
