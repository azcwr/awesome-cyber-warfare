# Corporate Surveillance

Gizmodo has a fascinating series [Goodbye Big Five](https://gizmodo.com/c/goodbye-big-five) about all of the hidden ways that the Big Five tech companies are involved in our lives and usually collecting our data.

* [Life Without the Tech Giants](https://gizmodo.com/life-without-the-tech-giants-1830258056)
* [I Tried to Block Amazon From My Life. It Was Impossible.](https://gizmodo.com/i-tried-to-block-amazon-from-my-life-it-was-impossible-1830565336)
* [Facebook Block: I Cut Facebook Out of My Life. I Missed It](https://gizmodo.com/i-cut-facebook-out-of-my-life-surprisingly-i-missed-i-1830565456)
* [I Cut Google Out Of My Life. It Screwed Up Everything](https://gizmodo.com/i-cut-google-out-of-my-life-it-screwed-up-everything-1830565500)
* [I Cut Microsoft Out of My Life—or So I Thought](https://gizmodo.com/i-cut-microsoft-out-of-my-life-or-so-i-thought-1830863898)
* [I Cut Apple Out of My Life. It Was Devastating](https://gizmodo.com/i-cut-apple-out-of-my-life-it-was-devastating-1831063868)
* [I Blocked Amazon, Facebook, Google, Microsoft, and Apple](https://gizmodo.com/i-cut-the-big-five-tech-giants-from-my-life-it-was-hel-1831304194)
* [How to Block Amazon, Google, Apple, Facebook, and Microsoft](https://gizmodo.com/want-to-really-block-the-tech-giants-heres-how-1832261612)
