![The Arizona Cyber Warfare Range](acwr2-high-resolution-dark.png)
# Awesome Cyber Warfare
A collection of cyber security and cyber warfare tools, techniques and resources.

Online Hackable Targets
--------------
[The Arizona Cyber Warfare Range](https://azcwr.org)

## Pages

* [Command Line and the Terminal](command-line.md)
* [Corporate Surveillance](corporate-surveillance.md)
* [Privacy and Ad Blocking](privacy-and-ad-blocking.md)

## Networking

* [DNS Tools](dns-tools.md)

## Programming

* [Serverless](serverless.md)

## Privacy and OpSec

* [awesome-privacy](https://github.com/KevinColemanInc/awesome-privacy) - Limiting personal data leaks on the internet
* [macOS-Security-and-Privacy-Guide](https://github.com/drduh/macOS-Security-and-Privacy-Guide) - Guide to securing and improving privacy on macOS. While this is macOS heavy, it's an extensive list, and a lot of it applies to other platforms, especially *nixes.
* [DVigneault/awesome-privacy](https://github.com/DVigneault/awesome-privacy)
* [joeperks/awesome-privacy](https://github.com/joeperks/awesome-privacy) - A list of privacy/digital rights related resources and organizations.
* [awesome-privacy-conferences](https://github.com/cryptoaustralia/awesome-privacy-conferences) - A curated list of upcoming privacy conferences

## Open Source Intelligence (OSINT)

* [OSINT Resources for 2019 – Steve Micallef](https://medium.com/@micallst/osint-resources-for-2019-b15d55187c3f)
* [awesome-osint](https://github.com/jivoi/awesome-osint) - A curated list of amazingly awesome OSINT

## Remote Job Runners

These programs can be used to run jobs on multiple hosts in parallel which could be very useful for password cracking (i.e. finding hash collisions), doing high volume network scans, or for diluting/hiding a network scan origin.

* [GNU Parallel](https://www.gnu.org/software/parallel/)
* [mmstick/parallel](https://github.com/mmstick/parallel) — Reimplementation of GNU Parallel in Rust
* [mmstick/concurr](https://github.com/mmstick/concurr) — Alternative to GNU Parallel w/ a client-server architecture

Many more [GNU Parallel alternatives](https://www.gnu.org/software/parallel/parallel_alternatives.html) from which to choose.

## High Performance Network Scanners

Nmap is usually all you need. But if you need to scan a whole /8 or even the entire IPv4 space, ZMap or masscan can get it done (assuming you have a large enough pipe like 10Gbit).

* [Nmap](https://nmap.org/) - The original and pretty fast for most tasks.
* [ZMap](https://github.com/zmap/zmap) -  is a fast single packet network scanner designed for Internet-wide network surveys.
* [masscan](https://github.com/robertdavidgraham/masscan) - TCP port scanner, spews SYN packets asynchronously, scanning entire Internet in under 5 minutes.

## Program Analysis

These repos are listed in decreasing order of how many resources they contain, as well as their relevance to the security aspects of program analysis.

* [awesome-malware-analysis](https://github.com/rshipp/awesome-malware-analysis) - An extensive list of malware analysis tools and resources.
* [awesome-dynamic-analysis](https://github.com/mre/awesome-dynamic-analysis) - Dynamic analysis tools to inspect the behavior of programs at runtime.
* [awesome-static-analysis](https://github.com/mre/awesome-static-analysis) - Static analysis tools to inspect the behavior of programs without running them, usually by inspecting their source code.

## Awesome Security Repos

Listed in order of where you should start/most general, and secondarily, decreasing order by how much content each contains.

* [awesome-security](https://github.com/sbilly/awesome-security) - A collection of awesome software, libraries, documents, books, resources and cools stuffs about security.
* [awesome-password-security](https://github.com/burrrata/awesome-password-security) - tools and strategies for users to create stronger and more memorable passwords
* [awesome-web-security](https://github.com/qazbnm456/awesome-web-security) -  A curated list of Web Security materials and resources.
* [osx-security-awesome](https://github.com/kai5263499/osx-security-awesome) - A collection of OSX and iOS security resources
* [osx-and-ios-security-awesome](https://github.com/ashishb/osx-and-ios-security-awesome) - OSX and iOS related security tools
* [android-security-awesome](https://github.com/ashishb/android-security-awesome) - A collection of android security related resources
* [awesome-ml-for-cybersecurity](https://github.com/jivoi/awesome-ml-for-cybersecurity) - Machine Learning for Cyber Security

A meta-list of security repositories for hackers, pentesters and security researchers [Awesome-Hacking](https://github.com/Hack-with-Github/Awesome-Hacking).

## More Awesome Repos

If it's not in one of the links above, it can probably be found via these list of awesome-* repos.

* [awesome-awesome](https://github.com/emijrp/awesome-awesome) - A curated list of awesome curated lists of many topics.
* [awesome-awesomeness](https://egrieco@github.com/bayandin/awesome-awesomeness) - A curated list of awesome awesomeness
* [lists](https://github.com/jnv/lists) - The definitive list of lists (of lists) curated on GitHub

Or going a level deeper, a curated list of curated lists of curated data: [awesome-awesome-awesome](https://github.com/jonatasbaldin/awesome-awesome-awesome)

Also the [GitHub Topic: awesome](https://github.com/topics/awesome) is another place to discover more curated lists.
